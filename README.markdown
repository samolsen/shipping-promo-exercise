# eBay Code Sample for Sam Olsen

## Background

This project demonstrates a solution to a code challenge problem designed by eBay. 
See the full [problem statement](docs/problem-statement.markdown).

## Getting started

### Prerequisites

The following dependencies are required:

##### Development dependencies

- [Java Development Kit 11](https://openjdk.java.net/projects/jdk/11/)
- [Maven](http://maven.apache.org) 3.6 or later
- [Docker](http://maven.apache.org), for integration tests 

##### Runtime dependencies

- [Java 11 Runtime Environment](https://openjdk.java.net/install/)
- [PostgreSQL](https://www.postgresql.org) 9.6 or later. A [Docker Compose](https://docs.docker.com/compose/install/) 
file is included at [./containers/docker-compose.yml](./containers/docker-compose.yml) which may be used 
to provide this dependency

## Build and test

Build the application with Apache Maven, from the project root:

```
./mvnw clean package
```

To include integration tests, build with the Maven `verify` goal:

```
./mvnw clean verify
```

## Run the application

### Prerequisites

A PostgreSQL instance must be running, and a database user must exist for the application which 
may create tables and indexes. A [Docker Compose](https://docs.docker.com/compose/install/) file is included at 
[./containers/docker-compose.yml](./containers/docker-compose.yml) which may be used to provide 
this dependency.

_Note_: The database schema will be automatically created by the application using 
[Flyway](https://flywaydb.org) on application start. 

### Launch the jar

Start the application with:

```
java -jar ./target/shipping-promo-exercise-${VERSION}.jar
```

#### Additional configuration

It may be necessary to override the application's [database connection attributes](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html#data-properties) 
or web host port. Spring Boot provides many [mechanisms](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config) 
for doing this. The most direct approach is to use additional command line arguments:

```
java -jar ./target/shipping-promo-exercise-${VERSION}.jar                   \
  --spring.datasource.url=jdbc:postgresql://localhost:5432/mydatabasename   \
  --spring.datasource.username=mydbuser                                     \
  --spring.datasource.password=mypassword                                   \
  --server.port=8080
```

#### Logging

The application will direct its output to a log file according to Spring Boot's [rules](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-logging).

This project sets the default log file location to `shipping-promo-exercise.log` in the current 
working directory, but the configuration may override this path. 

### Explore the API

RESTful HTTP APIs are documented using [Swagger](https://swagger.io). You may browse the Swagger UI 
from `http://localhost:8080/swagger-ui.html`
 
Swagger 2.0 API definitions may be loaded from `http://localhost:8080/v2/api-docs` for code 
generation or inspection with other tools.

 
### Approach and assumptions

All domain objects are modeled and configured in the application database. RESTful services are 
available to perform CRUD operations on each.

Securing the API transport layer, authentication, and authorization are assumed to be
outside the scope of this exercise. 

#### Domain objects

- **Item**: an item for sale
    - An item must have exactly one category and exactly one seller.
    - A seller's inventory/item quantity on hand is not considered.
    - An item may qualify for zero or many shipping promotions.
    - API responses model an item's categories, sellers, and shipping promotions as nested objects for better extensibility. 
- **Category**: a category for an item.
- **Seller**: a seller for an item.
- **Shipping Promotion**: a shipping promotion which applies to an item based on business rules.
    - A single shipping promotion may consider one or many categories, and one or many sellers.

### Quick-start script

A bash script (requiring `curl` and [`jq`](https://stedolan.github.io/jq/)) is provided 
[here](./docs/load-sample-data.sh) which may be used to load starter data via a running REST 
service.  