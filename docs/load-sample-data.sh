#!/usr/bin/env bash

HOST=localhost
PORT=8080

# Create Category
CATEGORY_ID=$(curl -X POST --header 'Content-type: application/json' \
    --data '{"name":"My Category"}' \
    http://$HOST:$PORT/api/categories | jq '.id' )

# Create Seller
SELLER_ID=$(curl -X POST --header 'Content-type: application/json' \
    --data '{"name":"My Seller"}' \
    http://$HOST:$PORT/api/sellers | jq '.id' )

# Create Item
curl -X POST --header 'Content-type: application/json' \
    --data "{\"name\":\"My Item\",\"price\":29.99,\"category\":{\"id\":${CATEGORY_ID}},\"seller\":{\"id\":$SELLER_ID}}" \
    http://$HOST:$PORT/api/items

# Create Shipping Promotion
PROMO_ID=$(curl -X POST --header 'Content-type: application/json' \
    --data '{"name":"My Promo","minimumPrice":25.00}' \
    http://$HOST:$PORT/api/shippingPromotions | jq '.id')

# Add Category to Promotion
curl -X POST --header 'Content-type: application/json' \
  --data "{\"id\":$CATEGORY_ID}" \
  http://$HOST:$PORT/api/shippingPromotions/$PROMO_ID/categories

# Add Seller to promotion
curl -X POST --header 'Content-type: application/json' \
  --data "{\"id\":$SELLER_ID}" \
  http://$HOST:$PORT/api/shippingPromotions/$PROMO_ID/sellers