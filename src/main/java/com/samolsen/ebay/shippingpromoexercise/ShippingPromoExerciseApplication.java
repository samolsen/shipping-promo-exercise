package com.samolsen.ebay.shippingpromoexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShippingPromoExerciseApplication {

  public static void main(String[] args) {
    SpringApplication.run(ShippingPromoExerciseApplication.class, args);
  }

}
