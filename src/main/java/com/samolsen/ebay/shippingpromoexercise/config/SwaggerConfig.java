package com.samolsen.ebay.shippingpromoexercise.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  /**
   * Provide a configuration which selects APIs for inclusion in Swagger 2.0 API documentation
   *
   * @return Swagger documentation object for SpringFox
   */
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(new ApiInfoBuilder()
            .title("eBay Code Sample for Sam Olsen")
            .description("<h2>Background</h2>\n"
                + " \n"
                + "<p>eBay would like to introduce a new regional shipping program to serve buyers and sellers in remote locations. To promote the inventory eligible for this new shipping program, eBay is planning to make changes to its front end user experiences and back end services. These changes will range from altering search rankings, to modifying recommendation algorithms, to changing the way eligible items are visually displayed.</p>\n"
                + " \n"
                + "<p>Each planned change (and probably some unplanned changes) depends upon a core service that determines whether or not an item is eligible for the new shipping program.  An item is determined to be eligible for this new program according to a set of rules that is created and managed by eBay administrators. The core set of rules will probably change over time, but initially includes all of the following:</p>\n"
                + " \n"
                + "<ul>\n"
                + "<li>The seller of the item is enrolled in the shipping program</li>\n"
                + "<li>the item is from a pre-approved category</li>\n"
                + "<li>the item has a price greater than or equal to some minimum amount</li>\n"
                + " \n"
                + "</ul>\n"
                + "\n"
                + "<h2>Goal</h2>\n"
                + " \n"
                + "<p>Write a service that identifies whether or not an item is eligible for the new shipping program.</p>\n"
                + " \n"
                + "<h2>Service API</h2>\n"
                + " \n"
                + "Service clients must provide the following input about an item:\n"
                + " \n"
                + "<ul>\n"
                + "<li><strong>title</strong>: A string representing the title of the item</li>\n"
                + "<li><strong>seller</strong>: A string containing the user name of the seller of the item</li>\n"
                + "<li><strong>category</strong>: An integer representing the numeric identifier of the item's category</li>\n"
                + "<li><strong>price</strong>: A double representing the price of the item in USD</li>\n"
                + "</ul>\n"
                + " \n"
                + "<p>In non-error conditions, the service must return an output value that indicates whether or not an item is eligible for the new service.</p>\n"
                + " \n"
                + "<h2>Rules Configuration</h2>\n"
                + " \n"
                + "<p>As business needs change, the eligibility rules will need to change as well.  Some changes will require modifying service code, such as adding a completely new rule (e.g. the program is only available in certain seasons).  Other changes should only require configuration modifications, such as increasing the minimum item price, adding/removing a user to the enrollment list for the program, or adding/removing a category to the pre-approved category list.  Top marks will be given for exposing these configurable items through an API that could be used as a basis for browser based tooling for eBay administrators.</p>")
            .build())
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.regex("^/api/.*"))
        .build();
  }
}
