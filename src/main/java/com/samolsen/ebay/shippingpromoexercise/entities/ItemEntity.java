package com.samolsen.ebay.shippingpromoexercise.entities;


import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * ORM object for a Item and its relations
 */
@Entity
@Table(name = "items")
public class ItemEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;
  @Column(name = "name")
  private String name;
  @Column(name = "price")
  private BigDecimal price;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "category_id")
  private CategoryEntity category;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "seller_id")
  private SellerEntity seller;

  @Transient
  private Set<ShippingPromotionEntity> shippingPromotions = new HashSet<>();

  protected ItemEntity() {
    /* JPA Constructor */
  }

  public ItemEntity(
      Integer id,
      String name,
      BigDecimal price,
      CategoryEntity category,
      SellerEntity seller,
      Set<ShippingPromotionEntity> shippingPromotions) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.category = category;
    this.seller = seller;
    this.shippingPromotions.addAll(shippingPromotions);
  }

  public ItemEntity(
      Integer id,
      String name,
      BigDecimal price,
      CategoryEntity category,
      SellerEntity seller) {
    this(id, name, price, category, seller, Collections.emptySet());
  }

  public ItemEntity(
      String name,
      BigDecimal price,
      CategoryEntity category,
      SellerEntity seller) {
    this(null, name, price, category, seller);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public CategoryEntity getCategory() {
    return category;
  }

  public void setCategory(CategoryEntity category) {
    this.category = category;
  }

  public SellerEntity getSeller() {
    return seller;
  }

  public void setSeller(SellerEntity seller) {
    this.seller = seller;
  }

  public Set<ShippingPromotionEntity> getShippingPromotions() {
    return shippingPromotions;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ItemEntity that = (ItemEntity) o;
    return Objects.equals(id, that.id) &&
        name.equals(that.name) &&
        price.compareTo(that.price) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, price.intValue());
  }
}
