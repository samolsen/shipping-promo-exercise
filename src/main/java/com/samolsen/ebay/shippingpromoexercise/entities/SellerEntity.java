package com.samolsen.ebay.shippingpromoexercise.entities;

import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * ORM object for a Seller and its relations
 */
@Entity
@Table(name = "sellers")
public class SellerEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;
  @Column(name = "name")
  private String name;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "seller")
  private Set<ItemEntity> items;

  protected SellerEntity() {
    /* JPA Constructor */
  }

  public SellerEntity(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  public SellerEntity(String name) {
    this(null, name);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<ItemEntity> getItems() {
    return items;
  }

  public void setItems(Set<ItemEntity> items) {
    this.items = items;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SellerEntity that = (SellerEntity) o;
    return Objects.equals(id, that.id) &&
        name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }
}
