package com.samolsen.ebay.shippingpromoexercise.entities;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * ORM object for a shipping promotion and its relations
 */
@Entity
@Table(name = "shipping_promotions")
public class ShippingPromotionEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;
  @Column(name = "name")
  private String name;
  @Column(name = "minimum_price")
  private BigDecimal minimumPrice;
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
      name = "shipping_promotion_sellers",
      joinColumns = @JoinColumn(name = "shipping_promotion_id"),
      inverseJoinColumns = @JoinColumn(name = "seller_id")
  )
  private Set<SellerEntity> eligibleSellers = new HashSet<>();
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
      name = "shipping_promotion_categories",
      joinColumns = @JoinColumn(name = "shipping_promotion_id"),
      inverseJoinColumns = @JoinColumn(name = "category_id")
  )
  private Set<CategoryEntity> eligibleCategories = new HashSet<>();

  protected ShippingPromotionEntity() {
    /* JPA Constructor */
  }

  public ShippingPromotionEntity(Integer id, String name, BigDecimal minimumPrice) {
    this.id = id;
    this.name = name;
    this.minimumPrice = minimumPrice;
  }

  public ShippingPromotionEntity(String name, BigDecimal minimumPrice) {
    this(null, name, minimumPrice);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getMinimumPrice() {
    return minimumPrice;
  }

  public void setMinimumPrice(BigDecimal minimumPrice) {
    this.minimumPrice = minimumPrice;
  }

  public Set<SellerEntity> getEligibleSellers() {
    return eligibleSellers;
  }

  public Set<CategoryEntity> getEligibleCategories() {
    return eligibleCategories;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ShippingPromotionEntity that = (ShippingPromotionEntity) o;
    return Objects.equals(id, that.id) &&
        name.equals(that.name) &&
        minimumPrice.compareTo(that.minimumPrice) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, minimumPrice.intValue());
  }
}
