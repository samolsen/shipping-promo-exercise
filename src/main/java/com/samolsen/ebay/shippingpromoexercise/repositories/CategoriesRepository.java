package com.samolsen.ebay.shippingpromoexercise.repositories;

import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriesRepository extends JpaRepository<CategoryEntity, Integer> {

}
