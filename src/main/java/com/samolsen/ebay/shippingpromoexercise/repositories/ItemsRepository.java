package com.samolsen.ebay.shippingpromoexercise.repositories;

import com.samolsen.ebay.shippingpromoexercise.entities.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemsRepository extends JpaRepository<ItemEntity, Integer> {

}
