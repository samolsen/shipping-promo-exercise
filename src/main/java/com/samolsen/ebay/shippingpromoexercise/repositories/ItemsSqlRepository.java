package com.samolsen.ebay.shippingpromoexercise.repositories;

import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ItemEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Custom repository interface retrieving {@link ItemEntity}s including any associated {@link
 * ShippingPromotionEntity}s.
 *
 * There's almost certainly a JPA-based solution which may provide these features.
 */
@Repository
public class ItemsSqlRepository {

  private static final Logger LOGGER = LoggerFactory.getLogger(ItemsSqlRepository.class);
  private final static String ITEM_SQL = "SELECT "
      + "    i.id             AS itemid,"
      + "    i.name           AS itemname,"
      + "    i.price          AS itemprice,"
      + "    i.category_id    AS categoryid,"
      + "    i.seller_id      AS sellerid ,"
      + "    s.name           AS sellername,"
      + "    c.name           AS categoryname,"
      + "    sp.id            AS promoid,"
      + "    sp.name          AS promoname,"
      + "    sp.minimum_price AS promominprice"
      + "  FROM items i"
      + "    JOIN sellers s ON s.id = i.seller_id"
      + "    JOIN categories c ON c.id = i.category_id"
      + "    LEFT JOIN shipping_promotion_categories spc on i.category_id = spc.category_id"
      + "    LEFT JOIN shipping_promotion_sellers sps on i.seller_id = sps.seller_id"
      + "    LEFT JOIN shipping_promotions sp"
      + "              on spc.shipping_promotion_id = sp.id AND sps.shipping_promotion_id = sp.id AND"
      + "                 sp.minimum_price <= i.price";

  private DataSource dataSource;

  @Autowired
  public ItemsSqlRepository(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  /**
   * Retrieve all items from the database
   *
   * @return list of items, with the transient field for associated shipping promotions populated
   */
  public List<ItemEntity> findAll() {
    try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(ITEM_SQL);
        ResultSet resultSet = statement.executeQuery()) {
      return extractDistinctEntities(resultSet);
    } catch (SQLException e) {
      LOGGER.error("Error retrieving items", e);
      throw new RuntimeException("Error retrieving items", e);
    }
  }


  /**
   * Retrieve a single item by its primary key
   *
   * @param id primary key
   * @return Optional wrapping an item if one exists, including transient shipping promotions
   * populated, otherwise an empty Optional
   */
  public Optional<ItemEntity> findById(int id) {
    String sql = ITEM_SQL + " WHERE i.id = ?";

    try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
    ) {
      statement.setInt(1, id);
      try (ResultSet resultSet = statement.executeQuery()) {
        List<ItemEntity> itemEntities = extractDistinctEntities(resultSet);
        if (itemEntities.size() > 1) {
          LOGGER.error("Logical error. Only one item can exist per id");
          throw new IllegalStateException("Logical error. Only one item can exist per id");
        }
        return itemEntities.isEmpty()
            ? Optional.empty()
            : Optional.of(itemEntities.get(0));
      }
    } catch (SQLException e) {
      LOGGER.error("Error retrieving items", e);
      throw new RuntimeException("Error retrieving items", e);
    }
  }

  /**
   * Parse the rows in a result set. Rows will exist per-shipping promotion. Collapse rows for the
   * same item into a single object, adding all shipping promotions to the item's set of
   * promotions.
   *
   * @param resultSet {@link ResultSet}
   * @return a list of parsed items, with shipping promotion info included
   */
  private List<ItemEntity> extractDistinctEntities(ResultSet resultSet) throws SQLException {
    Map<Integer, ItemEntity> itemsById = new HashMap<>();

    while (resultSet.next()) {
      ItemEntity row = extractItem(resultSet);
      ItemEntity itemEntity = itemsById.computeIfAbsent(row.getId(), (id) -> row);
      itemEntity.getShippingPromotions().addAll(row.getShippingPromotions());
    }
    return new ArrayList<>(itemsById.values());
  }

  private ItemEntity extractItem(ResultSet resultSet) throws SQLException {
    int itemId = resultSet.getInt("itemid");
    String itemName = resultSet.getString("itemname");
    BigDecimal itemPrice = new BigDecimal(resultSet.getString("itemprice"));
    CategoryEntity categoryEntity = extractCategory(resultSet);
    SellerEntity sellerEntity = extractSeller(resultSet);
    Set<ShippingPromotionEntity> shippingPromotions = extractShippingPromo(resultSet);

    return new ItemEntity(
        itemId,
        itemName,
        itemPrice,
        categoryEntity,
        sellerEntity,
        shippingPromotions);
  }

  private CategoryEntity extractCategory(ResultSet resultSet) throws SQLException {
    int id = resultSet.getInt("categoryid");
    String name = resultSet.getString("categoryname");
    return new CategoryEntity(id, name);
  }

  private SellerEntity extractSeller(ResultSet resultSet) throws SQLException {
    int id = resultSet.getInt("sellerid");
    String name = resultSet.getString("sellername");
    return new SellerEntity(id, name);
  }

  private Set<ShippingPromotionEntity> extractShippingPromo(ResultSet resultSet)
      throws SQLException {
    int id = resultSet.getInt("promoid");
    if (resultSet.wasNull()) { // not all items will have a shipping promotion
      return Collections.emptySet();
    }

    String name = resultSet.getString("promoname");
    BigDecimal minPrice = new BigDecimal(resultSet.getString("promominprice"));
    return Collections.singleton(new ShippingPromotionEntity(id, name, minPrice));
  }

}
