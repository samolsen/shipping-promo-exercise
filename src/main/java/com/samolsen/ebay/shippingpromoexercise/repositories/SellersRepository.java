package com.samolsen.ebay.shippingpromoexercise.repositories;

import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellersRepository extends JpaRepository<SellerEntity, Integer> {

}
