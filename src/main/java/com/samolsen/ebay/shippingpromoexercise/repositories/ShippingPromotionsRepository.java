package com.samolsen.ebay.shippingpromoexercise.repositories;

import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingPromotionsRepository extends
    JpaRepository<ShippingPromotionEntity, Integer> {
}
