package com.samolsen.ebay.shippingpromoexercise.rest;

import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ItemEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.CategoryDTO;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ItemDTO;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.SellerDTO;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ShippingPromotionDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ModelMapper {

  public ItemDTO toItemDTO(ItemEntity entity) {
    CategoryDTO category = toCategoryDTO(entity.getCategory());
    SellerDTO seller = toSellerDTO(entity.getSeller());
    return new ItemDTO(
        entity.getId(),
        entity.getName(),
        entity.getPrice(),
        category,
        seller,
        toShippingProgramDTOs(entity.getShippingPromotions()));
  }

  public ItemEntity toItemEntity(ItemDTO itemDTO) {
    return new ItemEntity(
        itemDTO.getId(),
        itemDTO.getName(),
        itemDTO.getPrice(),
        toCategoryEntity(itemDTO.getCategory()),
        toSellerEntity(itemDTO.getSeller()));
  }

  public SellerDTO toSellerDTO(SellerEntity entity) {
    return new SellerDTO(entity.getId(), entity.getName());
  }

  public SellerEntity toSellerEntity(SellerDTO sellerDTO) {
    return new SellerEntity(sellerDTO.getId(), sellerDTO.getName());
  }

  public CategoryDTO toCategoryDTO(CategoryEntity entity) {
    return new CategoryDTO(entity.getId(), entity.getName());
  }

  public CategoryEntity toCategoryEntity(CategoryDTO categoryDTO) {
    return new CategoryEntity(categoryDTO.getId(), categoryDTO.getName());
  }

  public ShippingPromotionDTO toShippingPromotionDTO(
      ShippingPromotionEntity entity,
      boolean includeAssociations) {
    return includeAssociations
        ? new ShippingPromotionDTO(entity.getId(), entity.getName(), entity.getMinimumPrice(),
        toCategoryDTOs(entity.getEligibleCategories()), toSellerDTOs(entity.getEligibleSellers()))
        : new ShippingPromotionDTO(entity.getId(), entity.getName(), entity.getMinimumPrice());
  }

  public ShippingPromotionDTO toShippingPromotionDTO(ShippingPromotionEntity entity) {
    return toShippingPromotionDTO(entity, false);
  }

  public ShippingPromotionEntity toShippingPromotionEntity(
      ShippingPromotionDTO shippingPromotionDTO) {
    return new ShippingPromotionEntity(shippingPromotionDTO.getId(), shippingPromotionDTO.getName(),
        shippingPromotionDTO.getMinimumPrice());
  }

  private Set<CategoryDTO> toCategoryDTOs(Set<CategoryEntity> categoryEntities) {
    return categoryEntities.stream().map(this::toCategoryDTO).collect(Collectors.toSet());
  }

  private Set<SellerDTO> toSellerDTOs(Set<SellerEntity> sellerEntities) {
    return sellerEntities.stream().map(this::toSellerDTO).collect(Collectors.toSet());
  }

  private Set<ShippingPromotionDTO> toShippingProgramDTOs(Set<ShippingPromotionEntity> entities) {
    return entities.stream()
        .map(this::toShippingPromotionDTO)
        .collect(Collectors.toSet());
  }


}
