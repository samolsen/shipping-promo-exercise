package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.CategoriesRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.ModelMapper;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.CategoryDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/categories")
@Api(tags = "Categories")
public class CategoriesController {

  private CategoriesRepository categoriesRepository;
  private ModelMapper modelMapper;

  @Autowired
  public CategoriesController(
      CategoriesRepository categoriesRepository,
      ModelMapper modelMapper) {
    this.categoriesRepository = categoriesRepository;
    this.modelMapper = modelMapper;
  }

  @GetMapping
  @ApiOperation(value = "Get all Categories")
  public List<CategoryDTO> getCategories() {
    List<CategoryEntity> categoryEntities = categoriesRepository.findAll();
    return categoryEntities.stream()
        .map(modelMapper::toCategoryDTO)
        .collect(Collectors.toList());
  }

  @GetMapping("/{id}")
  @ApiOperation(value = "Get Category by id")
  public CategoryDTO getCategory(
      @ApiParam(value = "Category id", name = "Category id", required = true)
      @PathVariable("id") int id) {
    CategoryEntity CategoryEntity = categoriesRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    return modelMapper.toCategoryDTO(CategoryEntity);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation(value = "Create Category")
  public CategoryDTO createCategory(@RequestBody CategoryDTO categoryDTO) {
    CategoryEntity entity = modelMapper.toCategoryEntity(categoryDTO);
    categoriesRepository.saveAndFlush(entity);
    return modelMapper.toCategoryDTO(entity);
  }

  @PutMapping("/{id}")
  @ApiOperation(value = "Update Category")
  public CategoryDTO updateCategory(
      @ApiParam(value = "Category id", name = "Category id", required = true)
      @PathVariable("id") int id,
      @RequestBody CategoryDTO categoryDTO) {
    if (id != categoryDTO.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    CategoryEntity entity = categoriesRepository.findById(categoryDTO.getId())
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    entity.setName(categoryDTO.getName());
    categoriesRepository.saveAndFlush(entity);
    return modelMapper.toCategoryDTO(entity);
  }

  @DeleteMapping("/{id}")
  @ApiOperation(value = "Delete Category")
  public ResponseEntity<Void> deleteCategory(
      @ApiParam(value = "Category id", name = "Category id", required = true)
      @PathVariable("id") int id) {
    categoriesRepository.deleteById(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

}
