package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import com.samolsen.ebay.shippingpromoexercise.entities.ItemEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.ItemsRepository;
import com.samolsen.ebay.shippingpromoexercise.repositories.ItemsSqlRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.ModelMapper;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ItemDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/items")
@Api(tags = "Items")
public class ItemsController {

  private ItemsRepository itemsRepository;
  private ItemsSqlRepository itemsSqlRepository;
  //  private CategoriesRepository categoriesRepository;
//  private SellersRepository sellersRepository;
  private ModelMapper modelMapper;

  @Autowired
  public ItemsController(
      ItemsRepository itemsRepository,
      ItemsSqlRepository itemsSqlRepository,
      ModelMapper modelMapper) {
    this.itemsRepository = itemsRepository;
    this.itemsSqlRepository = itemsSqlRepository;
    this.modelMapper = modelMapper;
  }

  @GetMapping
  @ApiOperation(value = "Get all Items")
  public List<ItemDTO> getItems() {
    List<ItemEntity> itemEntities = itemsSqlRepository.findAll();

    return itemEntities.stream()
        .map(modelMapper::toItemDTO)
        .collect(Collectors.toList());
  }

  @GetMapping("/{id}")
  @ApiOperation(value = "Get Item by id")
  public ItemDTO getItem(
      @ApiParam(value = "Item id", name = "Item id", required = true)
      @PathVariable("id") int id) {
    ItemEntity itemEntity = itemsSqlRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    return modelMapper.toItemDTO(itemEntity);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation("Create Item")
  public ItemDTO createItem(@RequestBody ItemDTO itemDTO) {
    ItemEntity itemEntity = modelMapper.toItemEntity(itemDTO);
    itemsRepository.saveAndFlush(itemEntity);
    // Retrieve the record via ItemsSqlRepository so shipping promotions are included
    return getItem(itemEntity.getId());
  }

  @PutMapping("/{id}")
  @ApiOperation(value = "Update Item")
  public ItemDTO updateItem(
      @ApiParam(value = "Item id", name = "Item id", required = true)
      @PathVariable("id") int id,
      @RequestBody ItemDTO itemDTO) {
    if (id != itemDTO.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    ItemEntity entity = itemsRepository.findById(itemDTO.getId())
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    entity.setName(itemDTO.getName());
    entity.setPrice(itemDTO.getPrice());
    entity.setCategory(modelMapper.toCategoryEntity(itemDTO.getCategory()));
    entity.setSeller(modelMapper.toSellerEntity(itemDTO.getSeller()));
    itemsRepository.saveAndFlush(entity);
    // Retrieve the record via ItemsSqlRepository so shipping promotions are included
    return getItem(entity.getId());
  }


  @DeleteMapping("/{id}")
  @ApiOperation("Delete Item")
  public ResponseEntity<Void> deleteItem(
      @ApiParam(value = "Item id", name = "Item id", required = true)
      @PathVariable("id") int id) {
    itemsRepository.deleteById(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }


}
