package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.SellersRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.ModelMapper;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.SellerDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/sellers")
@Api(tags = "Sellers")
public class SellersController {

  private SellersRepository sellersRepository;
  private ModelMapper modelMapper;

  @Autowired
  public SellersController(
      SellersRepository sellersRepository,
      ModelMapper modelMapper) {
    this.sellersRepository = sellersRepository;
    this.modelMapper = modelMapper;
  }

  @GetMapping
  @ApiOperation(value = "Get all Sellers")
  public List<SellerDTO> getSellers() {
    List<SellerEntity> sellerEntities = sellersRepository.findAll();
    return sellerEntities.stream()
        .map(modelMapper::toSellerDTO)
        .collect(Collectors.toList());
  }

  @GetMapping("/{id}")
  @ApiOperation(value = "Get Seller by id")
  public SellerDTO getSeller(
      @ApiParam(value = "Seller id", name = "Seller id", required = true)
      @PathVariable("id") int id) {
    SellerEntity sellerEntity = sellersRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    return modelMapper.toSellerDTO(sellerEntity);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation(value = "Create Seller")
  public SellerDTO createSeller(@RequestBody SellerDTO sellerDTO) {
    SellerEntity entity = modelMapper.toSellerEntity(sellerDTO);
    sellersRepository.saveAndFlush(entity);
    return modelMapper.toSellerDTO(entity);
  }

  @PutMapping("/{id}")
  @ApiOperation(value = "Update Seller")
  public SellerDTO updateSeller(
      @ApiParam(value = "Seller id", name = "Seller id", required = true)

      @PathVariable("id") int id,
      @RequestBody SellerDTO sellerDTO) {
    if (id != sellerDTO.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    SellerEntity entity = sellersRepository.findById(sellerDTO.getId())
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    entity.setName(sellerDTO.getName());
    sellersRepository.saveAndFlush(entity);
    return modelMapper.toSellerDTO(entity);
  }

  @DeleteMapping("/{id}")
  @ApiOperation("Delete Seller")
  public ResponseEntity<Void> deleteSeller(
      @ApiParam(value = "Seller id", name = "Seller id", required = true)
      @PathVariable("id") int id) {
    sellersRepository.deleteById(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

}
