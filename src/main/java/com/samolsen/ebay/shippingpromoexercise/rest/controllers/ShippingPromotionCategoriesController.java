package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.CategoriesRepository;
import com.samolsen.ebay.shippingpromoexercise.repositories.ShippingPromotionsRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.ModelMapper;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.CategoryDTO;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ShippingPromotionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/shippingPromotions/{id}/categories")
@Api(tags = "Shipping Promotions")
public class ShippingPromotionCategoriesController {

  private ShippingPromotionsRepository shippingPromotionsRepository;
  private CategoriesRepository categoriesRepository;
  private ModelMapper modelMapper;

  @Autowired
  public ShippingPromotionCategoriesController(
      ShippingPromotionsRepository shippingPromotionsRepository,
      CategoriesRepository categoriesRepository,
      ModelMapper modelMapper) {
    this.shippingPromotionsRepository = shippingPromotionsRepository;
    this.categoriesRepository = categoriesRepository;
    this.modelMapper = modelMapper;
  }

  @GetMapping
  @ApiOperation("Get Categories participating in a Shipping Promotion")
  public Set<CategoryDTO> getShippingPromotionCategories(
      @ApiParam("Shipping Promotion id")
      @PathVariable("id") int id) {
    ShippingPromotionEntity shippingPromotionEntity = shippingPromotionsRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "The shipping promotion does not exist"));
    return shippingPromotionEntity.getEligibleCategories().stream()
        .map(modelMapper::toCategoryDTO)
        .collect(Collectors.toSet());
  }

  @PostMapping
  @ApiOperation("Add Category to Shipping Promotion")
  public ShippingPromotionDTO addCategoryToPromo(
      @ApiParam("Shipping Promotion id")
      @PathVariable("id") int id,
      @RequestBody CategoryDTO categoryDTO) {
    ShippingPromotionEntity shippingPromotionEntity = shippingPromotionsRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "The shipping promotion does not exist"));

    CategoryEntity categoryEntity = categoriesRepository
        .findById(categoryDTO.getId())
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
            "The category does not exist"));

    shippingPromotionEntity.getEligibleCategories().add(categoryEntity);
    shippingPromotionsRepository.save(shippingPromotionEntity);

    return modelMapper.toShippingPromotionDTO(shippingPromotionEntity, true);
  }

  @DeleteMapping("/{categoryId}")
  @ApiOperation("Remove Category from a Shipping Promotion")
  public ShippingPromotionDTO removeCategoryFromPromo(
      @ApiParam("Shipping Promotion id") @PathVariable("id") int id,
      @ApiParam("Category id") @PathVariable("categoryId") int categoryId) {
    ShippingPromotionEntity shippingPromotionEntity = shippingPromotionsRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "The shipping promotion does not exist"));

    shippingPromotionEntity.getEligibleCategories()
        .removeIf(categoryEntity -> categoryEntity.getId().equals(categoryId));
    return modelMapper.toShippingPromotionDTO(shippingPromotionEntity, true);
  }
}
