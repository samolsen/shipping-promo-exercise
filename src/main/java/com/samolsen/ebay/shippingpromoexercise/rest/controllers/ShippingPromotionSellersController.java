package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.SellersRepository;
import com.samolsen.ebay.shippingpromoexercise.repositories.ShippingPromotionsRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.ModelMapper;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.SellerDTO;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ShippingPromotionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/shippingPromotions/{id}/sellers")
@Api(tags = "Shipping Promotions")
public class ShippingPromotionSellersController {

  private ShippingPromotionsRepository shippingPromotionsRepository;
  private SellersRepository sellersRepository;
  private ModelMapper modelMapper;

  @Autowired
  public ShippingPromotionSellersController(
      ShippingPromotionsRepository shippingPromotionsRepository,
      SellersRepository sellersRepository,
      ModelMapper modelMapper) {
    this.shippingPromotionsRepository = shippingPromotionsRepository;
    this.sellersRepository = sellersRepository;
    this.modelMapper = modelMapper;
  }

  @GetMapping
  @ApiOperation("Get Sellers participating in a Shipping Promotion")
  public Set<SellerDTO> getShippingPromotionSellers(
      @ApiParam("Shipping Promotion id")
      @PathVariable("id") int id) {
    ShippingPromotionEntity shippingPromotionEntity = shippingPromotionsRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "The shipping promotion does not exist"));
    return shippingPromotionEntity.getEligibleSellers().stream()
        .map(modelMapper::toSellerDTO)
        .collect(Collectors.toSet());
  }

  @PostMapping
  @ApiOperation("Add Seller to Shipping Promotion")
  public ShippingPromotionDTO addSellerToPromo(
      @ApiParam("Shipping Promotion id")
      @PathVariable("id") int id,
      @RequestBody SellerDTO sellerDTO) {
    ShippingPromotionEntity shippingPromotionEntity = shippingPromotionsRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "The shipping promotion does not exist"));

    SellerEntity sellerEntity = sellersRepository
        .findById(sellerDTO.getId())
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
            "The seller does not exist"));

    shippingPromotionEntity.getEligibleSellers().add(sellerEntity);
    shippingPromotionsRepository.save(shippingPromotionEntity);

    return modelMapper.toShippingPromotionDTO(shippingPromotionEntity, true);
  }

  @DeleteMapping("/{sellerId}")
  @ApiOperation("Remove Seller from a Shipping Promotion")
  public ShippingPromotionDTO removeSellerFromPromo(
      @ApiParam("Shipping Promotion id") @PathVariable("id") int id,
      @ApiParam("Seller id") @PathVariable("sellerId") int sellerId) {
    ShippingPromotionEntity shippingPromotionEntity = shippingPromotionsRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "The shipping promotion does not exist"));

    shippingPromotionEntity.getEligibleSellers()
        .removeIf(sellerEntity -> sellerEntity.getId().equals(sellerId));
    return modelMapper.toShippingPromotionDTO(shippingPromotionEntity, true);
  }
}
