package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.ShippingPromotionsRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.ModelMapper;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ShippingPromotionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/shippingPromotions")
@Api(tags = "Shipping Promotions")
public class ShippingPromotionsController {

  private ShippingPromotionsRepository shippingPromotionsRepository;
  private ModelMapper modelMapper;

  @Autowired
  public ShippingPromotionsController(
      ShippingPromotionsRepository shippingPromotionsRepository,
      ModelMapper modelMapper) {
    this.shippingPromotionsRepository = shippingPromotionsRepository;
    this.modelMapper = modelMapper;
  }

  @GetMapping
  @ApiOperation(value = "Get all Shipping Promotions")
  public List<ShippingPromotionDTO> getCategories() {
    List<ShippingPromotionEntity> shippingPromoEntities = shippingPromotionsRepository.findAll();
    return shippingPromoEntities.stream()
        .map(modelMapper::toShippingPromotionDTO)
        .collect(Collectors.toList());
  }

  @GetMapping("/{id}")
  @ApiOperation(value = "Get Shipping Promotion by id")
  public ShippingPromotionDTO getShippingPromotion(@PathVariable("id") int id) {
    ShippingPromotionEntity shippingPromotionEntity = shippingPromotionsRepository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    return modelMapper.toShippingPromotionDTO(shippingPromotionEntity);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ApiOperation(value = "Create Shipping Promotion")
  public ShippingPromotionDTO createShippingPromotion(
      @RequestBody ShippingPromotionDTO shippingPromotionDTO) {
    ShippingPromotionEntity entity = modelMapper.toShippingPromotionEntity(shippingPromotionDTO);
    shippingPromotionsRepository.saveAndFlush(entity);
    return modelMapper.toShippingPromotionDTO(entity);
  }

  @PutMapping("/{id}")
  @ApiOperation(value = "Update Shipping Promotion")
  public ShippingPromotionDTO updateShippingPromotion(
      @ApiParam(value = "Shipping Promotion id", name = "Shipping Promotion id", required = true)
      @PathVariable("id") int id,
      @RequestBody ShippingPromotionDTO shippingPromotionDTO) {
    if (id != shippingPromotionDTO.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    ShippingPromotionEntity entity = shippingPromotionsRepository
        .findById(shippingPromotionDTO.getId())
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    entity.setName(shippingPromotionDTO.getName());
    entity.setMinimumPrice(shippingPromotionDTO.getMinimumPrice());
    shippingPromotionsRepository.saveAndFlush(entity);
    return modelMapper.toShippingPromotionDTO(entity);
  }

  @DeleteMapping("/{id}")
  @ApiOperation(value = "Delete Shipping Promotion")
  public ResponseEntity<Void> deleteShippingPromotion(
      @ApiParam(value = "Shipping Promotion id", name = "Shipping Promotion id", required = true)
      @PathVariable("id") int id) {
    shippingPromotionsRepository.deleteById(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

}
