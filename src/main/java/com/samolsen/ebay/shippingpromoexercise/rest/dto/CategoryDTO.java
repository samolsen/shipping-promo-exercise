package com.samolsen.ebay.shippingpromoexercise.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * REST API data transfer object for a Category
 */
@ApiModel(value = "Category")
public class CategoryDTO {

  private Integer id;
  private String name;

  @JsonCreator
  public CategoryDTO(@JsonProperty("id") Integer id, @JsonProperty("name") String name) {
    this.id = id;
    this.name = name;
  }

  public CategoryDTO(String name) {
    this.name = name;
  }

  @ApiModelProperty(value = "Category id")
  public Integer getId() {
    return id;
  }

  @ApiModelProperty(value = "Category name", required = true)
  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CategoryDTO that = (CategoryDTO) o;
    return Objects.equals(id, that.id) &&
        name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }
}
