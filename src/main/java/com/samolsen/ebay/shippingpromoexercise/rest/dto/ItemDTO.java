package com.samolsen.ebay.shippingpromoexercise.rest.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * REST API data transfer object for an Item
 */
@ApiModel(value = "Item")
public class ItemDTO {

  private Integer id;
  private String name;
  private BigDecimal price;
  private CategoryDTO category;
  private SellerDTO seller;
  private Set<ShippingPromotionDTO> shippingPromotions = new HashSet<>();

  public ItemDTO(
      Integer id,
      String name,
      BigDecimal price,
      CategoryDTO category,
      SellerDTO seller,
      Set<ShippingPromotionDTO> shippingPromotions) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.category = category;
    this.seller = seller;
    this.shippingPromotions.addAll(shippingPromotions);
  }

  @JsonCreator
  public ItemDTO(
      @JsonProperty("id") Integer id,
      @JsonProperty("name") String name,
      @JsonProperty("price") BigDecimal price,
      @JsonProperty("category") CategoryDTO category,
      @JsonProperty("seller") SellerDTO seller) {
    this(id, name, price, category, seller, Collections.emptySet());
  }

  public ItemDTO(
      String name,
      BigDecimal price,
      CategoryDTO category,
      SellerDTO seller) {
    this(null, name, price, category, seller);
  }

  public Integer getId() {
    return id;
  }

  @ApiModelProperty(value = "Item name", required = true)
  public String getName() {
    return name;
  }

  @ApiModelProperty(value = "Item price", required = true)
  public BigDecimal getPrice() {
    return price;
  }

  @ApiModelProperty(value = "Item category", required = true)
  public CategoryDTO getCategory() {
    return category;
  }

  @ApiModelProperty(value = "Item seller", required = true)
  public SellerDTO getSeller() {
    return seller;
  }

  @ApiModelProperty(value = "Eligible shipping promotions")
  public Set<ShippingPromotionDTO> getShippingPromotions() {
    return shippingPromotions;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ItemDTO itemDTO = (ItemDTO) o;
    return Objects.equals(id, itemDTO.id) &&
        name.equals(itemDTO.name) &&
        price.equals(itemDTO.price) &&
        category.equals(itemDTO.category) &&
        seller.equals(itemDTO.seller) &&
        Objects.equals(shippingPromotions, itemDTO.shippingPromotions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, price, category, seller, shippingPromotions);
  }
}
