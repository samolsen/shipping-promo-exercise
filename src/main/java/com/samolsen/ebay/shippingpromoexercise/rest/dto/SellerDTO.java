package com.samolsen.ebay.shippingpromoexercise.rest.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * REST API data transfer object for a Seller
 */
@ApiModel(value = "Seller")
public class SellerDTO {

  private Integer id;
  private String name;

  @JsonCreator
  public SellerDTO(@JsonProperty("id") Integer id, @JsonProperty("name") String name) {
    this.id = id;
    this.name = name;
  }

  public SellerDTO(String name) {
    this(null, name);
  }

  @ApiModelProperty(value = "Seller id")
  public Integer getId() {
    return id;
  }

  @ApiModelProperty(value = "Seller name", required = true)
  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SellerDTO sellerDTO = (SellerDTO) o;
    return Objects.equals(id, sellerDTO.id) &&
        name.equals(sellerDTO.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }
}
