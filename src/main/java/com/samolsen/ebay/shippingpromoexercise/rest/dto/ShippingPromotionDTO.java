package com.samolsen.ebay.shippingpromoexercise.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

/**
 * REST API data transfer object for a Shipping Promotion
 */
@ApiModel(value = "ShippingPromotion")
public class ShippingPromotionDTO {

  private Integer id;
  private String name;
  private BigDecimal minimumPrice;
  private Set<CategoryDTO> categories;
  private Set<SellerDTO> sellers;


  public ShippingPromotionDTO(
      Integer id,
      String name,
      BigDecimal minimumPrice,
      Set<CategoryDTO> categories,
      Set<SellerDTO> sellers) {
    this.id = id;
    this.name = name;
    this.minimumPrice = minimumPrice;
    this.categories = categories;
    this.sellers = sellers;
  }

  @JsonCreator
  public ShippingPromotionDTO(
      @JsonProperty("id") Integer id,
      @JsonProperty("name") String name,
      @JsonProperty("minimumPrice") BigDecimal minimumPrice) {
    this(id, name, minimumPrice, null, null);
  }

  public ShippingPromotionDTO(
      String name,
      BigDecimal minimumPrice) {
    this(null, name, minimumPrice);
  }

  @ApiModelProperty(value = "Shipping promotion id")
  public Integer getId() {
    return id;
  }

  @ApiModelProperty(value = "Shipping promotion name", required = true)
  public String getName() {
    return name;
  }

  @ApiModelProperty(value = "Shipping promotion minimum price for qualifying items", required = true)
  public BigDecimal getMinimumPrice() {
    return minimumPrice;
  }

  @ApiModelProperty(
      value = "Categories eligible for this shipping promotion",
      notes = "Only included when managing shipping promotions",
      required = false)
  public Set<CategoryDTO> getCategories() {
    return categories;
  }

  @ApiModelProperty(
      value = "Sellers eligible for this shipping promotion",
      notes = "Only included when managing shipping promotions",
      required = false)
  public Set<SellerDTO> getSellers() {
    return sellers;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ShippingPromotionDTO that = (ShippingPromotionDTO) o;
    return Objects.equals(id, that.id) &&
        name.equals(that.name) &&
        minimumPrice.equals(that.minimumPrice);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, minimumPrice);
  }
}
