CREATE TABLE IF NOT EXISTS categories
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS sellers
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS items
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR        NOT NULL,
    price       NUMERIC(16, 2) NOT NULL,
    category_id INTEGER        NOT NULL REFERENCES categories (id),
    seller_id   INTEGER        NOT NULL REFERENCES sellers (id)
);
CREATE INDEX IF NOT EXISTS items_category_idx ON items (category_id);
CREATE INDEX IF NOT EXISTS items_seller_idx ON items (seller_id);

CREATE TABLE IF NOT EXISTS shipping_promotions
(
    id            SERIAL PRIMARY KEY,
    name          VARCHAR        NOT NULL,
    minimum_price NUMERIC(16, 2) NOT NULL
);
CREATE TABLE IF NOT EXISTS shipping_promotion_sellers
(
    id                    SERIAL PRIMARY KEY,
    shipping_promotion_id INTEGER NOT NULL REFERENCES shipping_promotions (id),
    seller_id             INTEGER NOT NULL REFERENCES sellers (id),
    UNIQUE (shipping_promotion_id, seller_id)
);
CREATE TABLE IF NOT EXISTS shipping_promotion_categories
(
    id                    SERIAL PRIMARY KEY,
    shipping_promotion_id INTEGER NOT NULL REFERENCES shipping_promotions (id),
    category_id           INTEGER NOT NULL REFERENCES categories (id),
    UNIQUE (shipping_promotion_id, category_id)
);