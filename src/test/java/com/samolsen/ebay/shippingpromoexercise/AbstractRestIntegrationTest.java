package com.samolsen.ebay.shippingpromoexercise;

import com.samolsen.ebay.shippingpromoexercise.AbstractRestIntegrationTest.Initializer;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = Initializer.class)
@Testcontainers
@DirtiesContext
public class AbstractRestIntegrationTest {

  @Container
  public static final PostgreSQLContainer DB = new PostgreSQLContainer();

  @LocalServerPort
  public Integer springPort;

  public static class Initializer
      implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
      TestPropertyValues.of(
          "spring.datasource.url=" + DB.getJdbcUrl(),
          "spring.datasource.username=" + DB.getUsername(),
          "spring.datasource.password=" + DB.getPassword()
      ).applyTo(configurableApplicationContext.getEnvironment());
    }
  }

  @BeforeEach
  public void beforeEach() {
    RestAssured.requestSpecification = new RequestSpecBuilder()
        .setPort(springPort)
        .setContentType(ContentType.JSON)
        .setAccept(ContentType.JSON)
        .build();
  }

}
