package com.samolsen.ebay.shippingpromoexercise.repositories;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ItemEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.ItemsSqlRepositoryIT.Initializer;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = Initializer.class)
@Testcontainers
@DirtiesContext
public class ItemsSqlRepositoryIT {

  /**
   * Run these tests against a separate db instance from RESTful service tests to provide a clean
   * context
   */
  @Container
  public static final PostgreSQLContainer DB = new PostgreSQLContainer();


  public static class Initializer
      implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
      TestPropertyValues.of(
          "spring.datasource.url=" + DB.getJdbcUrl(),
          "spring.datasource.username=" + DB.getUsername(),
          "spring.datasource.password=" + DB.getPassword()
      ).applyTo(configurableApplicationContext.getEnvironment());
    }
  }

  @Autowired
  ItemsSqlRepository sqlRepository;
  @Autowired
  CategoriesRepository categoriesRepository;
  @Autowired
  ItemsRepository itemsRepository;
  @Autowired
  SellersRepository sellersRepository;
  @Autowired
  ShippingPromotionsRepository shippingPromotionsRepository;


  CategoryEntity categoryEntity;
  SellerEntity sellerEntity;

  @BeforeEach
  public void beforeEach() {
    clearDatabase();
    categoryEntity = categoriesRepository.save(new CategoryEntity("Category"));
    sellerEntity = sellersRepository.save(new SellerEntity("Seller"));
  }

  @Test
  public void testGetItemWithoutPromo() {
    ItemEntity itemEntity = itemsRepository
        .save(new ItemEntity("Item 1", new BigDecimal("40.0"), categoryEntity, sellerEntity));
    ItemEntity dbEntity = sqlRepository.findById(itemEntity.getId()).orElseThrow();
    assertEquals(itemEntity, dbEntity);
    assertTrue(dbEntity.getShippingPromotions().isEmpty());
  }

  @Test
  public void testGetItemWithQualifyingPromo() {
    ItemEntity itemEntity = itemsRepository
        .save(new ItemEntity("Item 2", new BigDecimal("40.0"), categoryEntity, sellerEntity));
    ShippingPromotionEntity shippingPromotion = createShippingPromo(
        "Promo 2", new BigDecimal("25.0"), categoryEntity, sellerEntity);

    ItemEntity dbEntity = sqlRepository.findById(itemEntity.getId()).orElseThrow();
    assertEquals(itemEntity, dbEntity);
    assertTrue(dbEntity.getShippingPromotions().contains(shippingPromotion));
  }

  @Test
  public void testGetItemWithQualifyingPromoEqualPrice() {
    BigDecimal edgeCasePrice = new BigDecimal("35.00");
    ItemEntity itemEntity = itemsRepository
        .save(new ItemEntity("Item 2", edgeCasePrice, categoryEntity, sellerEntity));
    ShippingPromotionEntity shippingPromotion = createShippingPromo(
        "Promo 2", edgeCasePrice, categoryEntity, sellerEntity);

    ItemEntity dbEntity = sqlRepository.findById(itemEntity.getId()).orElseThrow();
    assertEquals(itemEntity, dbEntity);
    assertTrue(dbEntity.getShippingPromotions().contains(shippingPromotion));
  }

  @Test
  public void testGetItemWitMultipleQualifyingPromo() {
    ItemEntity itemEntity = itemsRepository
        .save(new ItemEntity("Item 2", new BigDecimal("40.0"), categoryEntity, sellerEntity));
    ShippingPromotionEntity shippingPromotion = createShippingPromo(
        "One", new BigDecimal("25.0"), categoryEntity, sellerEntity);
    ShippingPromotionEntity anotherPromotion = createShippingPromo(
        "Two", new BigDecimal("40.0"), categoryEntity, sellerEntity);

    ItemEntity dbEntity = sqlRepository.findById(itemEntity.getId()).orElseThrow();
    assertEquals(itemEntity, dbEntity);
    assertTrue(dbEntity.getShippingPromotions().contains(shippingPromotion));
    assertTrue(dbEntity.getShippingPromotions().contains(anotherPromotion));
  }

  @Test
  public void testGetItemWithNonQualifyingPromoPrice() {
    ItemEntity itemEntity = itemsRepository
        .save(new ItemEntity("Item 2", new BigDecimal("40.0"), categoryEntity, sellerEntity));
    ShippingPromotionEntity shippingPromotion = createShippingPromo(
        "Promo 2", new BigDecimal("41.0"), categoryEntity, sellerEntity);

    ItemEntity dbEntity = sqlRepository.findById(itemEntity.getId()).orElseThrow();
    assertEquals(itemEntity, dbEntity);
    assertFalse(dbEntity.getShippingPromotions().contains(shippingPromotion));
  }

  @Test
  public void testGetItemWithNonQualifyingPromoSeller() {
    ItemEntity itemEntity = itemsRepository
        .save(new ItemEntity("Item 2", new BigDecimal("40.0"), categoryEntity, sellerEntity));
    SellerEntity otherSeller = sellersRepository.save(new SellerEntity("Other"));
    ShippingPromotionEntity shippingPromotion = createShippingPromo(
        "Promo 2", new BigDecimal("25.0"), categoryEntity, otherSeller);

    ItemEntity dbEntity = sqlRepository.findById(itemEntity.getId()).orElseThrow();
    assertEquals(itemEntity, dbEntity);
    assertFalse(dbEntity.getShippingPromotions().contains(shippingPromotion));
  }

  @Test
  public void testGetItemWithNonQualifyingPromoCategory() {
    ItemEntity itemEntity = itemsRepository
        .save(new ItemEntity("Item 2", new BigDecimal("40.0"), categoryEntity, sellerEntity));
    CategoryEntity otherCategory = categoriesRepository.save(new CategoryEntity("Other"));
    ShippingPromotionEntity shippingPromotion = createShippingPromo(
        "Promo 2", new BigDecimal("25.0"), otherCategory, sellerEntity);

    ItemEntity dbEntity = sqlRepository.findById(itemEntity.getId()).orElseThrow();
    assertEquals(itemEntity, dbEntity);
    assertFalse(dbEntity.getShippingPromotions().contains(shippingPromotion));
  }

  @Test
  public void testGetItemsWithoutPromo() {
    ItemEntity itemEntity = itemsRepository
        .save(new ItemEntity("Item 1", new BigDecimal("40.0"), categoryEntity, sellerEntity));
    ItemEntity itemEntity2 = itemsRepository
        .save(new ItemEntity("Item 2", new BigDecimal("40.0"), categoryEntity, sellerEntity));

    List<ItemEntity> dbEntities = sqlRepository.findAll();
    assertTrue(dbEntities.contains(itemEntity));
    assertTrue(dbEntities.contains(itemEntity2));
  }

  @Test
  public void testGetItemsWithPromos() {
    ItemEntity promoItem = itemsRepository
        .save(new ItemEntity("Item 1", new BigDecimal("40.0"), categoryEntity, sellerEntity));
    ItemEntity noPromoItem = itemsRepository
        .save(new ItemEntity("Item 2", new BigDecimal("25.0"), categoryEntity, sellerEntity));
    ShippingPromotionEntity shippingPromotion = createShippingPromo(
        "Promo Eleven", new BigDecimal("30.0"), categoryEntity, sellerEntity);

    List<ItemEntity> dbEntities = sqlRepository.findAll();

    ItemEntity dbEntityPromo = dbEntities.stream().filter(promoItem::equals).findAny()
        .orElseThrow();
    assertTrue(dbEntityPromo.getShippingPromotions().contains(shippingPromotion));

    ItemEntity dbEntityNoPromo = dbEntities.stream().filter(noPromoItem::equals).findAny()
        .orElseThrow();
    assertFalse(dbEntityNoPromo.getShippingPromotions().contains(shippingPromotion));
  }


  private ShippingPromotionEntity createShippingPromo(String name, BigDecimal price) {
    ShippingPromotionEntity shippingPromotionEntity = new ShippingPromotionEntity(name, price);
    return shippingPromotionsRepository.save(shippingPromotionEntity);
  }

  private ShippingPromotionEntity createShippingPromo(
      String name,
      BigDecimal price,
      CategoryEntity categoryEntity,
      SellerEntity sellerEntity) {
    ShippingPromotionEntity shippingPromotionEntity = createShippingPromo(name, price);
    shippingPromotionEntity.getEligibleCategories().add(categoryEntity);
    shippingPromotionEntity.getEligibleSellers().add(sellerEntity);
    return shippingPromotionsRepository.save(shippingPromotionEntity);
  }

  private void clearDatabase() {
    itemsRepository.deleteAll();
    shippingPromotionsRepository.deleteAll();
    categoriesRepository.deleteAll();
    sellersRepository.deleteAll();
  }
}
