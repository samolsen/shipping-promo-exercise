package com.samolsen.ebay.shippingpromoexercise.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ItemEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.CategoryDTO;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ItemDTO;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.SellerDTO;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ShippingPromotionDTO;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

public class ModelMapperTest {

  private ModelMapper modelMapper = new ModelMapper();

  @Test
  public void toItemDTOTest() {
    ItemEntity itemEntity = createItemEntity();
    ItemDTO itemDTO = modelMapper.toItemDTO(itemEntity);
    assertEquals(itemEntity.getId(), itemDTO.getId());
    assertEquals(itemEntity.getName(), itemDTO.getName());
    assertEquals(itemEntity.getPrice(), itemDTO.getPrice());
    assertEquals(itemEntity.getCategory().getId(), itemDTO.getCategory().getId());
    assertEquals(itemEntity.getSeller().getId(), itemDTO.getSeller().getId());
  }

  @Test
  public void testToItemEntity() {
    ItemDTO itemDTO = createItemDTO();
    ItemEntity itemEntity = modelMapper.toItemEntity(itemDTO);
    assertEquals(itemDTO.getId(), itemEntity.getId());
    assertEquals(itemDTO.getName(), itemEntity.getName());
    assertEquals(itemDTO.getPrice(), itemEntity.getPrice());
    assertEquals(itemDTO.getCategory().getId(), itemEntity.getCategory().getId());
    assertEquals(itemDTO.getSeller().getId(), itemEntity.getSeller().getId());
  }

  @Test
  public void toSellerDTOTest() {
    SellerEntity sellerEntity = createSellerEntity();
    SellerDTO sellerDTO = modelMapper.toSellerDTO(sellerEntity);
    assertEquals(sellerEntity.getId(), sellerDTO.getId());
    assertEquals(sellerEntity.getName(), sellerDTO.getName());
  }

  @Test
  public void toSellerEntityTest() {
    SellerDTO sellerDTO = createSellerDTO();
    SellerEntity sellerEntity = modelMapper.toSellerEntity(sellerDTO);
    assertEquals(sellerDTO.getId(), sellerEntity.getId());
    assertEquals(sellerDTO.getName(), sellerEntity.getName());
  }

  @Test
  public void toCategoryDTOTest() {
    CategoryEntity categoryEntity = createCategoryEntity();
    CategoryDTO categoryDTO = modelMapper.toCategoryDTO(categoryEntity);
    assertEquals(categoryEntity.getId(), categoryDTO.getId());
    assertEquals(categoryEntity.getName(), categoryDTO.getName());
  }

  @Test
  public void toCategoryEntityTest() {
    CategoryDTO categoryDTO = createCategoryDTO();
    CategoryEntity categoryEntity = modelMapper.toCategoryEntity(categoryDTO);
    assertEquals(categoryDTO.getId(), categoryEntity.getId());
    assertEquals(categoryDTO.getName(), categoryEntity.getName());
  }

  @Test
  public void toShippingPromotionDTOTest() {
    ShippingPromotionEntity shippingPromotionEntity = createShippingPromotionEntity();
    ShippingPromotionDTO shippingPromotionDTO = modelMapper
        .toShippingPromotionDTO(shippingPromotionEntity);
    assertEquals(shippingPromotionEntity.getId(), shippingPromotionDTO.getId());
    assertEquals(shippingPromotionEntity.getName(), shippingPromotionDTO.getName());
    assertEquals(shippingPromotionEntity.getMinimumPrice(), shippingPromotionDTO.getMinimumPrice());
  }

  @Test
  public void toShippingPromotionDTOIncludingAssociationsTest() {
    ShippingPromotionEntity shippingPromotionEntity = createShippingPromotionEntity();

    ShippingPromotionDTO shippingPromotionDTO = modelMapper
        .toShippingPromotionDTO(shippingPromotionEntity, true);
    assertEquals(
        shippingPromotionEntity.getEligibleCategories().size(),
        shippingPromotionDTO.getCategories().size());
    assertEquals(
        shippingPromotionEntity.getEligibleSellers().size(),
        shippingPromotionDTO.getSellers().size());

    shippingPromotionDTO = modelMapper.toShippingPromotionDTO(shippingPromotionEntity, false);
    assertNull(shippingPromotionDTO.getCategories());
    assertNull(shippingPromotionDTO.getSellers());
  }

  @Test
  public void toShippingPromotionEntityTest() {
    ShippingPromotionDTO shippingPromotionDTO = createShippingPromotionDTO();
    ShippingPromotionEntity shippingPromotionEntity = modelMapper
        .toShippingPromotionEntity(shippingPromotionDTO);
    assertEquals(shippingPromotionDTO.getId(), shippingPromotionEntity.getId());
    assertEquals(shippingPromotionDTO.getName(), shippingPromotionEntity.getName());
    assertEquals(shippingPromotionDTO.getMinimumPrice(), shippingPromotionEntity.getMinimumPrice());
  }

  private ItemEntity createItemEntity() {
    return new ItemEntity(
        55,
        "Artisanal Cheese",
        new BigDecimal("9.99"),
        createCategoryEntity(),
        createSellerEntity());
  }

  private ItemDTO createItemDTO() {
    return new ItemDTO(3,
        "Cheddar",
        new BigDecimal("7.25"),
        createCategoryDTO(),
        createSellerDTO());
  }

  private CategoryEntity createCategoryEntity() {
    return new CategoryEntity(17, "Dairy");
  }

  private SellerEntity createSellerEntity() {
    return new SellerEntity(82, "Sam's Fromage");
  }

  private CategoryDTO createCategoryDTO() {
    return new CategoryDTO(12, "Frozen Food");
  }

  private SellerDTO createSellerDTO() {
    return new SellerDTO(44, "Sarah's Confections");
  }

  private ShippingPromotionDTO createShippingPromotionDTO() {
    return new ShippingPromotionDTO(4, "Free Shipping 50", new BigDecimal("50.0"));
  }

  private ShippingPromotionEntity createShippingPromotionEntity() {
    return new ShippingPromotionEntity(4, "Free Shipping 100", new BigDecimal("100.0"));
  }

}
