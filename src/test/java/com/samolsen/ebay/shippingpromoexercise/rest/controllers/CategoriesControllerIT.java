package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.samolsen.ebay.shippingpromoexercise.AbstractRestIntegrationTest;
import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.CategoriesRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.CategoryDTO;
import io.restassured.response.Response;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class CategoriesControllerIT extends AbstractRestIntegrationTest {

  @Autowired
  CategoriesRepository categoriesRepository;

  @Test
  public void testGetAllCategories() {
    CategoryEntity categoryEntity = createCategory("Rocks and Minerals");

    CategoryDTO[] results = when().get("/api/categories")
        .then()
        .statusCode(HttpStatus.OK.value())
        .extract().response().as(CategoryDTO[].class);

    CategoryDTO createdResult = Arrays.stream(results)
        .filter(dto -> dto.getId().equals(categoryEntity.getId()))
        .findAny().orElse(null);
    assertNotNull(createdResult);
  }

  @Test
  public void testGetCategoryById() {
    CategoryEntity categoryEntity = createCategory("Bread Flours");
    when().get("/api/categories/{id}", categoryEntity.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(categoryEntity.getId()), "name", is(categoryEntity.getName()));
  }

  @Test
  public void testCreateCategory() {
    CategoryDTO categoryDTO = new CategoryDTO("Waffles");

    Response response = given().body(categoryDTO)
        .when().post("/api/categories")
        .then()
        .statusCode(HttpStatus.CREATED.value())
        .body(
            "id", greaterThan(0),
            "name", is(categoryDTO.getName()))
        .extract().response();

    Integer id = response.path("id");
    assertNotNull(categoriesRepository.findById(id).orElse(null));
  }

  @Test
  public void testUpdateCategory() {
    CategoryEntity categoryEntity = createCategory("Waffles");

    CategoryDTO categoryDTO = new CategoryDTO(categoryEntity.getId(), "Breakfast foods");
    given().body(categoryDTO)
        .when().put("/api/categories/{id}", categoryDTO.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(categoryEntity.getId()),
            "name", is(categoryDTO.getName()));

    categoryEntity = categoriesRepository.findById(categoryEntity.getId()).orElseThrow();
    assertEquals(categoryDTO.getName(), categoryEntity.getName());
  }

  @Test
  public void testUpdateCategoryMismatchedIds() {
    CategoryDTO categoryDTO = new CategoryDTO(987, "Golf");
    given().body(categoryDTO)
        .when().put("/api/categories/{id}", categoryDTO.getId() - 1)
        .then()
        .statusCode(HttpStatus.BAD_REQUEST.value());
  }

  @Test
  public void testDeleteCategory() {
    CategoryEntity categoryEntity = createCategory("Medical Supplies");
    when().delete("/api/categories/{id}", categoryEntity.getId())
        .then()
        .statusCode(HttpStatus.NO_CONTENT.value());
    assertNull(categoriesRepository.findById(categoryEntity.getId()).orElse(null));
  }

  private CategoryEntity createCategory(String name) {
    CategoryEntity categoryEntity = new CategoryEntity(name);
    categoriesRepository.saveAndFlush(categoryEntity);
    return categoryEntity;
  }

}
