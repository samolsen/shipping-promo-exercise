package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.samolsen.ebay.shippingpromoexercise.AbstractRestIntegrationTest;
import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ItemEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.CategoriesRepository;
import com.samolsen.ebay.shippingpromoexercise.repositories.ItemsRepository;
import com.samolsen.ebay.shippingpromoexercise.repositories.SellersRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.ModelMapper;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.CategoryDTO;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ItemDTO;
import io.restassured.response.Response;
import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class ItemsControllerIT extends AbstractRestIntegrationTest {

  @Autowired
  ItemsRepository itemsRepository;
  @Autowired
  CategoriesRepository categoriesRepository;
  @Autowired
  SellersRepository sellersRepository;
  ModelMapper modelMapper = new ModelMapper();

  @Test
  public void testGetAllItems() {
    ItemEntity itemEntity = createItem("A new item", BigDecimal.ONE, "The cat", "Sold by");

    ItemDTO[] results = when().get("/api/items")
        .then()
        .statusCode(HttpStatus.OK.value())
        .extract().response().as(ItemDTO[].class);

    ItemDTO createdResult = Arrays.stream(results)
        .filter(dto -> dto.getId().equals(itemEntity.getId()))
        .findAny().orElse(null);
    assertNotNull(createdResult);
  }

  @Test
  public void testGetItemById() {
    ItemEntity itemEntity = createItem(
        "My Item",
        new BigDecimal("3.20"),
        "The Category",
        "The Seller");

    when().get("/api/items/{id}", itemEntity.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(itemEntity.getId()),
            "name", is(itemEntity.getName()));
  }


  @Test
  public void testCreateItem() {
    CategoryEntity categoryEntity = createCategory("Sports");
    SellerEntity sellerEntity = createSeller("Above The Rim LLC");

    ItemDTO itemDTO = new ItemDTO(
        "Basketball",
        new BigDecimal("27.99"),
        modelMapper.toCategoryDTO(categoryEntity),
        modelMapper.toSellerDTO(sellerEntity));

    Response response = given().body(itemDTO)
        .when().post("/api/items")
        .then()
        .statusCode(HttpStatus.CREATED.value())
        .body(
            "id", greaterThan(0),
            "name", is(itemDTO.getName()),
            "price", is(itemDTO.getPrice().floatValue()),
            "category.id", is(categoryEntity.getId()),
            "seller.id", is(sellerEntity.getId()))
        .extract().response();

    Integer id = response.path("id");
    assertNotNull(itemsRepository.findById(id).orElse(null));
  }

  @Test
  public void testUpdateItem() {
    ItemEntity itemEntity = createItem(
        "Item 2",
        new BigDecimal("5.99"),
        "Old Category",
        "Old Seller");
    CategoryEntity categoryEntity = createCategory("New Category");
    SellerEntity sellerEntity = createSeller("New Seller");

    ItemDTO itemDTO = new ItemDTO(
        itemEntity.getId(),
        "New Name",
        new BigDecimal("18.02"),
        modelMapper.toCategoryDTO(categoryEntity),
        modelMapper.toSellerDTO(sellerEntity));
    given().body(itemDTO)
        .when().put("/api/items/{id}", itemDTO.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(itemEntity.getId()),
            "name", is(itemDTO.getName()),
            "price", is(itemDTO.getPrice().floatValue()),
            "category.id", is(categoryEntity.getId()),
            "seller.id", is(sellerEntity.getId()));

    itemEntity = itemsRepository.findById(itemDTO.getId()).orElseThrow();
    assertEquals(itemDTO.getName(), itemEntity.getName());
    assertEquals(itemDTO.getPrice(), itemEntity.getPrice());
    assertEquals(itemDTO.getCategory().getId(), itemEntity.getCategory().getId());
    assertEquals(itemDTO.getSeller().getId(), itemEntity.getSeller().getId());
  }

  @Test
  public void testUpdateItemMismatchedIds() {
    CategoryDTO categoryDTO = new CategoryDTO(987, "Golf");
    given().body(categoryDTO)
        .when().put("/api/items/{id}", categoryDTO.getId() - 1)
        .then()
        .statusCode(HttpStatus.BAD_REQUEST.value());
  }

  @Test
  public void testDeleteItem() {
    ItemEntity itemEntity = createItem(
        "Frying Pan",
        new BigDecimal("34.00"),
        "Kitchen",
        "Restaurant Warehouse");
    when().delete("/api/items/{id}", itemEntity.getId())
        .then()
        .statusCode(HttpStatus.NO_CONTENT.value());
    assertNull(itemsRepository.findById(itemEntity.getId()).orElse(null));
  }

  private ItemEntity createItem(
      String name,
      BigDecimal price,
      String categoryName,
      String sellerName) {
    ItemEntity itemEntity = new ItemEntity(
        name,
        price,
        createCategory(categoryName),
        createSeller(sellerName));
    itemsRepository.saveAndFlush(itemEntity);
    return itemEntity;
  }

  private CategoryEntity createCategory(String name) {
    return categoriesRepository.saveAndFlush(new CategoryEntity(name));
  }

  private SellerEntity createSeller(String name) {
    return sellersRepository.saveAndFlush(new SellerEntity(name));
  }

}
