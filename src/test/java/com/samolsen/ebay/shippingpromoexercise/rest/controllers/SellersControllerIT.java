package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.samolsen.ebay.shippingpromoexercise.AbstractRestIntegrationTest;
import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.SellersRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.SellerDTO;
import io.restassured.response.Response;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class SellersControllerIT extends AbstractRestIntegrationTest {

  @Autowired
  SellersRepository sellersRepository;

  @Test
  public void testGetAllSellers() {
    SellerEntity sellerEntity = createSeller("ACME");

    SellerDTO[] results = when().get("/api/sellers")
        .then()
        .statusCode(HttpStatus.OK.value())
        .extract().response().as(SellerDTO[].class);

    SellerDTO createdResult = Arrays.stream(results)
        .filter(dto -> dto.getId().equals(sellerEntity.getId()))
        .findAny().orElse(null);
    assertNotNull(createdResult);
  }

  @Test
  public void testGetSellerById() {
    SellerEntity sellerEntity = createSeller("Slightly Used Books");
    when().get("/api/sellers/{id}", sellerEntity.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(sellerEntity.getId()), "name", is(sellerEntity.getName()));
  }

  @Test
  public void testCreateSeller() {
    SellerDTO sellerDTO = new SellerDTO("ACME Products");

    Response response = given().body(sellerDTO)
        .when().post("/api/sellers")
        .then()
        .statusCode(HttpStatus.CREATED.value())
        .body(
            "id", greaterThan(0),
            "name", is(sellerDTO.getName()))
        .extract().response();

    Integer id = response.path("id");
    assertNotNull(sellersRepository.findById(id).orElse(null));
  }

  @Test
  public void testUpdateSeller() {
    SellerEntity sellerEntity = createSeller("Dunder Mifflin");

    SellerDTO sellerDTO = new SellerDTO(sellerEntity.getId(), "Scranton Paper");
    given().body(sellerDTO)
        .when().put("/api/sellers/{id}", sellerDTO.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(sellerEntity.getId()),
            "name", is(sellerDTO.getName()));

    sellerEntity = sellersRepository.findById(sellerEntity.getId()).orElseThrow();
    assertEquals(sellerDTO.getName(), sellerEntity.getName());
  }

  @Test
  public void testUpdateSellerMismatchedIds() {
    SellerDTO sellerDTO = new SellerDTO(987, "Sock Depot");
    given().body(sellerDTO)
        .when().put("/api/sellers/{id}", sellerDTO.getId() - 1)
        .then()
        .statusCode(HttpStatus.BAD_REQUEST.value());
  }

  @Test
  public void testDeleteSeller() {
    SellerEntity sellerEntity = createSeller("Sammy's Shrimp Co.");
    when().delete("/api/sellers/{id}", sellerEntity.getId())
        .then()
        .statusCode(HttpStatus.NO_CONTENT.value());
    assertNull(sellersRepository.findById(sellerEntity.getId()).orElse(null));
  }

  private SellerEntity createSeller(String name) {
    SellerEntity sellerEntity = new SellerEntity(name);
    sellersRepository.saveAndFlush(sellerEntity);
    return sellerEntity;
  }

}
