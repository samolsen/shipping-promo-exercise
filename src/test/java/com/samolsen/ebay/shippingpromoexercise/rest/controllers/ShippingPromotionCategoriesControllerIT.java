package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.samolsen.ebay.shippingpromoexercise.AbstractRestIntegrationTest;
import com.samolsen.ebay.shippingpromoexercise.entities.CategoryEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.CategoriesRepository;
import com.samolsen.ebay.shippingpromoexercise.repositories.ShippingPromotionsRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.ModelMapper;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.CategoryDTO;
import java.math.BigDecimal;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class ShippingPromotionCategoriesControllerIT extends AbstractRestIntegrationTest {

  @Autowired
  ShippingPromotionsRepository shippingPromotionsRepository;
  @Autowired
  CategoriesRepository categoriesRepository;
  @Autowired
  ModelMapper modelMapper;

  ShippingPromotionEntity shippingPromotionEntity;
  CategoryEntity categoryEntity;

  @BeforeEach
  public void beforeEach() {
    super.beforeEach();

    shippingPromotionEntity = new ShippingPromotionEntity(
        "ACME",
        new BigDecimal("234.56"));
    shippingPromotionsRepository.saveAndFlush(shippingPromotionEntity);

    categoryEntity = new CategoryEntity("ABC");
    categoriesRepository.saveAndFlush(categoryEntity);
  }

  @Test
  public void testGetShippingPromotionCategories() {
    shippingPromotionEntity.getEligibleCategories().add(categoryEntity);
    shippingPromotionsRepository.saveAndFlush(shippingPromotionEntity);

    CategoryDTO[] results = when()
        .get("/api/shippingPromotions/{id}/categories", shippingPromotionEntity.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .extract().response().as(CategoryDTO[].class);

    assertEquals(categoryEntity.getId(), results[0].getId());
  }

  @Test
  public void testAddCategoryToPromo() {
    CategoryDTO sellerDTO = modelMapper.toCategoryDTO(categoryEntity);
    given().body(sellerDTO)
        .when().post("/api/shippingPromotions/{id}/categories", shippingPromotionEntity.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(shippingPromotionEntity.getId()), "categories", Matchers.not(empty()));
  }


  @Test
  public void testRemoveCategoryFromPromo() {
    shippingPromotionEntity.getEligibleCategories().add(categoryEntity);
    shippingPromotionsRepository.saveAndFlush(shippingPromotionEntity);

    when()
        .delete("/api/shippingPromotions/{id}/categories/{categoryId}",
            shippingPromotionEntity.getId(), categoryEntity.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(shippingPromotionEntity.getId()), "categories", empty());
  }


}
