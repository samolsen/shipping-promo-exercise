package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.samolsen.ebay.shippingpromoexercise.AbstractRestIntegrationTest;
import com.samolsen.ebay.shippingpromoexercise.entities.SellerEntity;
import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.SellersRepository;
import com.samolsen.ebay.shippingpromoexercise.repositories.ShippingPromotionsRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.ModelMapper;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.SellerDTO;
import java.math.BigDecimal;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class ShippingPromotionSellersControllerIT extends AbstractRestIntegrationTest {

  @Autowired
  ShippingPromotionsRepository shippingPromotionsRepository;
  @Autowired
  SellersRepository sellersRepository;
  @Autowired
  ModelMapper modelMapper;

  ShippingPromotionEntity shippingPromotionEntity;
  SellerEntity sellerEntity;

  @BeforeEach
  public void beforeEach() {
    super.beforeEach();

    shippingPromotionEntity = new ShippingPromotionEntity(
        "ACME",
        new BigDecimal("234.56"));
    shippingPromotionsRepository.saveAndFlush(shippingPromotionEntity);

    sellerEntity = new SellerEntity("ABC");
    sellersRepository.saveAndFlush(sellerEntity);
  }

  @Test
  public void testGetShippingPromotionSellers() {
    shippingPromotionEntity.getEligibleSellers().add(sellerEntity);
    shippingPromotionsRepository.saveAndFlush(shippingPromotionEntity);

    SellerDTO[] results = when()
        .get("/api/shippingPromotions/{id}/sellers", shippingPromotionEntity.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .extract().response().as(SellerDTO[].class);

    assertEquals(sellerEntity.getId(), results[0].getId());
  }

  @Test
  public void testAddSellerToPromo() {
    SellerDTO sellerDTO = modelMapper.toSellerDTO(sellerEntity);
    given().body(sellerDTO)
        .when().post("/api/shippingPromotions/{id}/sellers", shippingPromotionEntity.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(shippingPromotionEntity.getId()), "sellers", Matchers.not(empty()));
  }


  @Test
  public void testRemoveCategoryFromPromo() {
    shippingPromotionEntity.getEligibleSellers().add(sellerEntity);
    shippingPromotionsRepository.saveAndFlush(shippingPromotionEntity);

    when()
        .delete("/api/shippingPromotions/{id}/sellers/{sellerId}",
            shippingPromotionEntity.getId(), sellerEntity.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(shippingPromotionEntity.getId()), "sellers", empty());
  }


}
