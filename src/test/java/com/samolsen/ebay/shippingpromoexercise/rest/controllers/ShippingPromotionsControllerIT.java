package com.samolsen.ebay.shippingpromoexercise.rest.controllers;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.samolsen.ebay.shippingpromoexercise.AbstractRestIntegrationTest;
import com.samolsen.ebay.shippingpromoexercise.entities.ShippingPromotionEntity;
import com.samolsen.ebay.shippingpromoexercise.repositories.ShippingPromotionsRepository;
import com.samolsen.ebay.shippingpromoexercise.rest.dto.ShippingPromotionDTO;
import io.restassured.response.Response;
import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class ShippingPromotionsControllerIT extends AbstractRestIntegrationTest {

  @Autowired
  ShippingPromotionsRepository shippingPromotionsRepository;

  @Test
  public void testGetAllShippingPromotions() {
    ShippingPromotionEntity shippingPromotionEntity = createShippingPromotion("A good promo",
        new BigDecimal("4"));

    ShippingPromotionDTO[] results = when().get("/api/shippingPromotions")
        .then()
        .statusCode(HttpStatus.OK.value())
        .extract().response().as(ShippingPromotionDTO[].class);

    ShippingPromotionDTO createdResult = Arrays.stream(results)
        .filter(dto -> dto.getId().equals(shippingPromotionEntity.getId()))
        .findAny().orElse(null);
    assertNotNull(createdResult);
  }

  @Test
  public void testGetShippingPromotionById() {
    ShippingPromotionEntity shippingPromotion = createShippingPromotion("Extra Special",
        new BigDecimal("49.99"));
    when().get("/api/shippingPromotions/{id}", shippingPromotion.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body(
            "id", is(shippingPromotion.getId()),
            "name", is(shippingPromotion.getName()),
            "minimumPrice", is(shippingPromotion.getMinimumPrice().floatValue()));
  }

  @Test
  public void testCreateShippingPromotion() {
    ShippingPromotionDTO shippingPromotionDTO = new ShippingPromotionDTO(
        "Free shipping",
        new BigDecimal("25.00"));

    Response response = given().body(shippingPromotionDTO)
        .when().post("/api/shippingPromotions")
        .then()
        .statusCode(HttpStatus.CREATED.value())
        .body(
            "id", greaterThan(0),
            "name", is(shippingPromotionDTO.getName()),
            "minimumPrice", is(shippingPromotionDTO.getMinimumPrice().floatValue()))
        .extract().response();

    Integer id = response.path("id");
    assertNotNull(shippingPromotionsRepository.findById(id).orElse(null));
  }

  @Test
  public void testUpdateShippingPromotion() {
    ShippingPromotionEntity shippingPromotionEntity = createShippingPromotion(
        "Half off",
        new BigDecimal("123.45"));

    ShippingPromotionDTO shippingPromotionDTO = new ShippingPromotionDTO(
        shippingPromotionEntity.getId(), "60% off", new BigDecimal("98.76"));
    given().body(shippingPromotionDTO)
        .when().put("/api/shippingPromotions/{id}", shippingPromotionDTO.getId())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("id", is(shippingPromotionEntity.getId()),
            "name", is(shippingPromotionDTO.getName()),
            "minimumPrice", is(shippingPromotionDTO.getMinimumPrice().floatValue()));

    shippingPromotionEntity = shippingPromotionsRepository.findById(shippingPromotionEntity.getId())
        .orElseThrow();
    assertEquals(shippingPromotionDTO.getName(), shippingPromotionEntity.getName());
    assertEquals(shippingPromotionDTO.getMinimumPrice(), shippingPromotionEntity.getMinimumPrice());
  }

  @Test
  public void testUpdateShippingPromotionMismatchedIds() {
    ShippingPromotionDTO shippingPromotionDTO = new ShippingPromotionDTO(4, "Savings Bonanza",
        new BigDecimal("35.00"));
    given().body(shippingPromotionDTO)
        .when().put("/api/shippingPromotions/{id}", shippingPromotionDTO.getId() - 1)
        .then()
        .statusCode(HttpStatus.BAD_REQUEST.value());
  }

  @Test
  public void testDeleteShippingPromotion() {
    ShippingPromotionEntity shippingPromotionEntity = createShippingPromotion("Delete Me",
        new BigDecimal("45"));
    when().delete("/api/shippingPromotions/{id}", shippingPromotionEntity.getId())
        .then()
        .statusCode(HttpStatus.NO_CONTENT.value());
    assertNull(shippingPromotionsRepository.findById(shippingPromotionEntity.getId()).orElse(null));
  }

  private ShippingPromotionEntity createShippingPromotion(String name, BigDecimal minimumPrice) {
    ShippingPromotionEntity shippingPromotionEntity = new ShippingPromotionEntity(
        name,
        minimumPrice);
    shippingPromotionsRepository.saveAndFlush(shippingPromotionEntity);
    return shippingPromotionEntity;
  }

}
